<footer class="content-info" role="contentinfo">
  <div class="container">
  	<br>
  	Dr. Steven Rice &copy; <?php echo date("Y"); ?>  |  Call us at <a href="tel:2157918881">(215) 791-8881</a>  |  Visit our office at 1601 Walnut St #1310, Philadelphia, PA 19102    
	<br>
  	<a href="http://drstevenrice.com/bellavista/">Bella Vista</a>, <a href="http://drstevenrice.com/centercity/">Center City</a>, <a href="http://drstevenrice.com/chinatown/">Chinatown</a>, <a href="http://drstevenrice.com/logansquare/">Logan Square</a>, <a href="http://drstevenrice.com/oldcity/">Old City</a>, <a href="http://drstevenrice.com/rittenhousesdental/">Rittenhouse Square</a>, <a href="http://drstevenrice.com/societyhill/">Society Hill</a>, <a href="http://drstevenrice.com/universitycity/">University City</a>, <a href="http://drstevenrice.com/washingtonsquarewest/">Washington Square West</a>, <a href=" http://drstevenrice.com/graduate-hospital/">Graduate Hospital</a>, <a href=" http://drstevenrice.com/fairmount-dentist/">Fairmount</a>      	
    <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
</footer>
<p>&nbsp;</p>