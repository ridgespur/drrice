<?php
/*
Template Name: Landing Page 1
*/

?>



<div class="jumbotron bg-rice bg">
	
</div>


<br>

<div class="row text-center">
	<h1 class="text-blue">Dr. Steven J Rice <br> Dental Services</h1>
	<!-- <h2 class="lead text-blue text-center">(215) 557-1555</h2> -->
	<h2><a href="tel:2155571555" onclick="_gaq.push(['_trackEvent','Phone Call Tracking','Click/Touch','Mobile']);">
	(215) 557-1555
	</a></h2>
</div>

<hr class="featurette-divider">

<div class="row row-centered">
	<div class="col-sm-6 col-centered">
		<img class="img-circle centered" src="http://drstevenrice.com/wp-content/uploads/2013/02/Dentist-Steven-Rice-Philadelphia.jpg">
	</div>
</div>
	
<p>&nbsp;</p>

<div class="row">
	<div class="col-lg-12">
		<p class="lead text-justify">Looking for a new dentist in Philadelphia? Look no further. Dr. Steven Rice and his team are among the most sought after dental professionals in the area. With a philosophy of providing every patient with dental health that lasts long after they leave our office and optimum comfort and relaxation, you’ll see why Dr. Rice and his team consistently receive high praise from past and current clients. Our team of friendly, caring dental professionals are highly trained in the latest techniques in virtually "pain-free" care. <b>Call us at (215) 557-1555 to schedule an appointment</b>.</p>
	</div>
</div>



<hr class="featurette-divider">

<div class="row">
	<div class="col-sm-12">
		<iframe src="https://www.google.com/maps/embed?pb=!1m27!1m12!1m3!1d3058.617794254589!2d-75.16975901948629!3d39.94993818022119!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m12!1i0!3e6!4m5!1s0x89c6c63074adc4db%3A0x7b99b7e129e8b704!2sDr.+Steven+Rice%2C+Walnut+Street%2C+Philadelphia%2C+PA!3m2!1d39.950142!2d-75.16767899999999!4m3!3m2!1d39.950007!2d-75.16769099999999!5e0!3m2!1sen!2sus!4v1410811721967" frameborder="0" style="border:0"></iframe>
		<p>You'll find us just a few blocks away from City Hall at 1601 Walnut St #1310, Philadelphia, PA 19102. </p>
	</div>
</div>

<a class="btn btn-success btn-lg btn-block" role="button" href="http://drstevenrice.com/contact-us">Schedule an Appointment</a>

<div class="col-lg-6">
<p>We Participate With:<br>
Aetna PPO
Delta Dental PPO
Cigna (Connecticut General) PPO
United Concordia PPO (Except PFT)</p>
</div>

<div class="col-lg-6">
<b>We Also Accept:</b><br>
AIG
American Postal Workers Union (APWU)
Blue Cross of NJ
Blue Cross of PA
Fortis Benefits
Guardian
Highmark Blue Shield
Horizon Blue Cross Blue Shield
Humana
MetLife
Principal Financial Group
United Medical Resources
United States Life Ins. Co
</div>
