<?php
/*
Template Name: Home Template
*/
?>

<?php get_template_part('templates/carousel'); ?>

<p>&nbsp;</p>

<div class="row">
	<div class="col-lg-3">
		<img class="img-circle" src="http://drstevenrice.com/wp-content/uploads/2013/02/Dentist-Steven-Rice-Philadelphia.jpg">
	</div>

	<div class="col-lg-9">
		<h1 class="text-blue">Dr. Steven J Rice <br> Dental Services</h1>
		<h2><a href="tel:2157918881">(215) 791-8881</a></h2>
	</div>
</div>

<hr class="featurette-divider">

<p>&nbsp;</p>

<div class="row">
	<div class="col-lg-12">
		<p class="lead text-justify">Looking for a new dentist in Philadelphia? Look no further. Dr. Steven Rice and his team are among the most sought after dental professionals in the area. With a philosophy of providing every patient with dental health that lasts long after they leave our office and optimum comfort and relaxation, you’ll see why Dr. Rice and his team consistently receive high praise from past and current clients. Our team of friendly, caring dental professionals are highly trained in the latest techniques in virtually "pain-free" care. <b>Call us at <a href="tel:2157918881">(215) 791-8881</a> to schedule an appointment</b>.</p>
	</div>
</div>

<hr class="featurette-divider">

<div class="row">
	<div class="col-lg-6">
		<h3>Our Office</h3>
		<p class="lead text-justify">When you take your first step in the door of our Center City office, you'll see a smiling receptionist and plenty of natural light. Your comfort is our priority, and we see 95% of patients within 5 to 10 minutes.</p>
	</div>
	
	<div class="col-lg-6">
		<h3>Ask a Question</h3>
		<p class="lead text-justify">When you take your first step in the door of our Center City office, you'll see a smiling receptionist and plenty of natural light. Your comfort is our priority, and we see 95% of patients within 5 to 10 minutes.</p>
		<p><a class="btn btn-lg btn-primary" href="/contact-us" role="button">Contact Us</a></p>
	</div>
</div>

<hr class="featurette-divider">

<div class="row">
	<div class="col-sm-12">
		<p class="text-center"><b>You'll find us just a few blocks away from City Hall at 1601 Walnut St #1310, Philadelphia, PA 19102.</b></p>
		<div class="embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m27!1m12!1m3!1d3058.617794254589!2d-75.16975901948629!3d39.94993818022119!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m12!1i0!3e6!4m5!1s0x89c6c63074adc4db%3A0x7b99b7e129e8b704!2sDr.+Steven+Rice%2C+Walnut+Street%2C+Philadelphia%2C+PA!3m2!1d39.950142!2d-75.16767899999999!4m3!3m2!1d39.950007!2d-75.16769099999999!5e0!3m2!1sen!2sus!4v1410811721967" frameborder="0" style="border:0"></iframe>
		</div>
	</div>
</div>

<p>&nbsp;</p>

<a class="btn btn-primary btn-lg btn-block" role="button" href="http://drstevenrice.com/contact-us">Schedule an Appointment</a>

<p>&nbsp;</p>

<?php while (have_posts()) : the_post(); ?>
  <?php //get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
